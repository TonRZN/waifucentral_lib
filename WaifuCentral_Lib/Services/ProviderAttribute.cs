﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaifuCentral_Lib.Services
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ProviderAttribute : Attribute
    {
        public string ProviderID { get; }
        private string _desc = null;

        public ProviderAttribute(string providerId)
        {
            ProviderID = providerId;
        }

        public string Description
        {
            get => _desc ?? ProviderID;
            set => _desc = value;
        }
    }
}
