﻿using HTML_Scrapper.Core;
using HTML_Scrapper.Helper;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WaifuCentral_Lib.Database;
using WaifuCentral_Lib.Structure;

namespace WaifuCentral_Lib.Services.Hardcoded
{
 
   [Provider("MAL", Description = "MyAnimeList Provider")]
   public class MAL : IProvider
    {
        string CharacterQueryURL { get => @"https://myanimelist.net/character.php?q="; }
        string CharacterPicturesBaseURL { get => @"https://cdn.myanimelist.net/images/characters/"; }
        List<string> AllowedNodes = new List<string> { "p", "i", "b", "br", "strong" };
        Logger Log = LogManager.GetCurrentClassLogger();

        public HtmlDocument SearchPage(string name)
        {
            try
            {
                return HTMLScrapper.GetDocumentFromURL(HTMLScrapper.GetElementAttribute(HTMLScrapper.GetDocumentFromURL($"{CharacterQueryURL}{name.Replace(" ", "%20")}", Encoding.UTF8), "//td[2]/a", "href"));
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return null;
            }
        }

        public string GetName(object html)
        {
            return HTMLScrapper.GetElementAttribute((html as HtmlDocument), @"//meta[@property=""og:title""]", "content");
        }

        public string GetNativeName(object html)
        {
            try
            {
                return HTMLScrapper.GetNode((html as HtmlDocument), @"//div[contains(@class,""breadcrumb"")]").NextSibling.SelectSingleNode(@"//span/small").InnerText.Replace("(", "").Replace(")", "");
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return null;
            }
        }

        public string GetAlternativeTitles(object html)
        {
            string result = HTMLScrapper.GetElementContent((html as HtmlDocument), "//h1").DeEntitize();

            if (result.Contains("\""))
                result = Regex.Replace(result, @".*""(.*)""", "$1");

            return result;
        }

        public string GetDescription(object html)
        {
            HtmlNode node = HTMLScrapper.GetNode((html as HtmlDocument), @"//*[@id=""content""]/table/tr/td[2]");

            foreach (HtmlNode n in node?.ChildNodes.Where(n => n.NodeType != HtmlNodeType.Text && !n.GetClasses().Contains("spoiler") && !AllowedNodes.Contains(n.Name)).ToList())
            {
                node.RemoveChild(n);
            }

            return node.InnerText.DeEntitize().Trim();
        }

        public string GetImage(object html)
        {
            return HTMLScrapper.GetElementAttribute((html as HtmlDocument), @"//meta[@property=""og:image""]", "content");
        }

        public JArray GetImages(object html)
        {
            JArray result = new JArray();

            HtmlNodeCollection nodes = null;

            try
            {
                nodes = (html as HtmlDocument).DocumentNode.SelectNodes(@"//div[contains(@class,""picSurround"")]/a");
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return null;
            }

            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("href", "NA") != "NA" && node.GetAttributeValue("href", "NA").Contains(CharacterPicturesBaseURL))
                    result.Add(node.GetAttributeValue("href", "NA"));
            }

            return result;
        }

        public string GetSeries(object html)
        {
            // Gets the first manga of the list
            string result = HTMLScrapper.GetNode((html as HtmlDocument), @"//div[contains(text(), 'Mangaography')]/../table[2]/tr").SelectSingleNode("./td[2]/a").InnerText;

            // If failed to get first manga, get the first anime
            if (String.IsNullOrWhiteSpace(result))
                result = HTMLScrapper.GetNode((html as HtmlDocument), @"//div[contains(text(), 'Animeography')]/../table[1]/tr").SelectSingleNode("./td[2]/a").InnerText;

            return result;
        }

        public string GetSource(object html)
        {
            return HTMLScrapper.GetElementAttribute((html as HtmlDocument), @"//meta[@property=""og:url""]", "content");
        }

        public Waifu Retrieve(object html)
        {
            Waifu result = new Waifu();
            TaskFactory factory = new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);

            var idTask = factory.StartNew(() => { result.Source = GetSource(html); });
            var nameTask = factory.StartNew(() => { result.Name = GetName(html); });
            var nativeNameTask = factory.StartNew(() => { result.NativeName = GetNativeName(html); });

            var alternativeTitlesTask = factory.StartNew(() => {
                string titles = GetAlternativeTitles(html);
                if (!result.Name.Equals(titles))
                    result.AlternativeTitles = titles;
                else
                    result.AlternativeTitles = "NA";
            });

            var descriptionTask = factory.StartNew(() => { result.Description = GetDescription(html); });
            var seriesTask = factory.StartNew(() => { result.Series = GetSeries(html); });

            JsonMergeSettings jsonMerge = new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Union };
            var imageTask = factory.StartNew(() => { result.Images.Merge(GetImage(html), jsonMerge); });
            var additionalImagesTask = factory.StartNew(() => { result.Images.Merge(GetImages(HTMLScrapper.GetDocumentFromURL($"{GetSource(html)}/pictures")), jsonMerge); });

            Task.WaitAll(idTask, nameTask, nativeNameTask, alternativeTitlesTask, descriptionTask, seriesTask, imageTask, additionalImagesTask);

            return result;
        }

        public Waifu Search(string name) => Retrieve(SearchPage(name));

        public Waifu Search(string name, Database_Helper helper)
        {
            if(helper.WaifuExists(name) == 0)
                Retrieve(SearchPage(name));

            return null;
        }

    }
}
