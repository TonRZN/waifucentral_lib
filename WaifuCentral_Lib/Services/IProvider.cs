﻿using Newtonsoft.Json.Linq;
using System;
using WaifuCentral_Lib.Structure;

namespace WaifuCentral_Lib.Services
{
    public interface IProvider
    {
        public string GetName(object param);

        public string GetNativeName(object param);
        public string GetAlternativeTitles(object param);

        public string GetDescription(object param);

        public string GetImage(object param);

        public JArray GetImages(object param);

        public string GetSeries(object param);

        public Waifu Search(string name);

        public string GetSource(object param);
    }
}
