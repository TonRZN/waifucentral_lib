﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WaifuCentral_Lib.Services
{
    public class ProviderLoader
    {
        public static IDictionary<string, Type> CustomProviders = new Dictionary<string, Type>();
        static Logger Log = LogManager.GetCurrentClassLogger();

        public static IProvider GetProvider(string id)
        {
            try
            {
                return (IProvider)Activator.CreateInstance(CustomProviders[id]);
            }
            catch
            {

            }

            return null;
        }

        public static void LoadProviders()
        {
            List<Assembly> assemblies = new List<Assembly>();

            Assembly assembly = Assembly.GetExecutingAssembly();
            assemblies.Add(Assembly.GetCallingAssembly());

            UriBuilder uri = new UriBuilder(assembly.GetName().CodeBase);
            string dirname = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path + "CustomProviders"));

            foreach (string dll in Directory.GetFiles(dirname, $"*_Provider.dll", SearchOption.AllDirectories))
            {
                try
                {
                    assemblies.Add(Assembly.LoadFile(dll));
                }
                catch (FileLoadException)
                {
                }
                catch (BadImageFormatException)
                {
                }
            }

            var implementations = assemblies.SelectMany(a => a.GetTypes())
                .Where(a => a.GetInterfaces().Contains(typeof(IProvider)));

            foreach (var implementation in implementations)
            {
                IEnumerable<ProviderAttribute> attributes = implementation.GetCustomAttributes<ProviderAttribute>();
                foreach ((string key, string desc) in attributes.Select(a => (key: a.ProviderID, desc: a.Description)))
                {
                    if (key == null) continue;
                    if (CustomProviders.ContainsKey(key))
                    {
                        Log.Warn($"[PROVIDER] Warning Duplicate provider key \"{key}\" of types {implementation}@{implementation.Assembly.Location} and {CustomProviders[key]}@{CustomProviders[key].Assembly.Location}");
                        continue;
                    }
                    CustomProviders.Add(key, implementation);
                    Log.Info($"Loaded custom provider: {key}");
                }
            }
        }


    }
}
