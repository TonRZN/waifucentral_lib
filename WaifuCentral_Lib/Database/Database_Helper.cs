﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using NLog;
using SQLite;
using WaifuCentral_Lib.Structure;

namespace WaifuCentral_Lib.Database
{
    public class Database_Helper
    {
        internal SQLiteConnection _database;
        Logger Log = LogManager.GetCurrentClassLogger();

        public Database_Helper(string path)
        {
            this._database = new SQLiteConnection(path);
        }

        public void CreateTables()
        {
            _database.CreateTable<Waifu>();
            _database.CreateTable<Owner>();
            _database.CreateTable<Claim>();
        }

        #region Waifu
        public void InsertWaifu(Waifu waifu)
        {
            _database.Insert(waifu);
        }

        public void InsertWaifu(string name, string series, string nativename = "NA", string alternativetitles = "NA", string description = "NA", JArray images = null, string source = "COMMUNITY")
        {
            _database.Insert(new Waifu(name, nativename, alternativetitles, series, description, images, source));
        }

        public Waifu RetrieveWaifu(int id)
        {
            Waifu result = null;

            try
            {
                result = _database.Table<Waifu>().Where(w => w.ID == id).First();
            }
            catch (Exception e)
            {
                Log.Error($"Could not find Waifu from the given id ({id}) \n Exception: {e}");
            }


            return result;
        }

        public List<Waifu> RetrieveWaifu(string name)
        {
            try
            {
                return _database.Table<Waifu>().Where(w => w.Name.ToLower().Contains(name.ToLower())).ToList();
            }
            catch (Exception e)
            {
                Log.Error($"Could not create Waifu from the given name ({name}) \n Exception: {e}");
            }


            return null;
        }

        public int WaifuExists(string name)
        {
            try
            {
                return _database.Table<Waifu>().Where(w => w.Name.Contains(name)).First().ID;
            }
            catch (Exception e)
            {
                Log.Error($"Could not find Waifu from the given name ({name}) \n Exception: {e}");
            }

            return 0;
        }

        public bool DeleteWaifu(int id)
        {
            bool result = false;

            try
            {
                result = _database.Delete<Waifu>(id) != 0;
            }
            catch (Exception e)
            {
                Log.Error($"Could not delete Waifu from the given id ({id}) \n Exception: {e}");
            }

            return result;
        }

        public bool DeleteWaifu(string name)
        {
            bool result = false;

            try
            {
                result = _database.Delete<Waifu>(WaifuExists(name)) != 0;
            }
            catch (Exception e)
            {
                Log.Error($"Could not delete Waifu from the given name ({name}) \n Exception: {e}");
            }

            return result;
        }
        #endregion

        #region Owner
        public void InsertOwner(Owner owner)
        {
            _database.Insert(owner);
        }

        public Owner RetrieveOwner(int id)
        {
            return _database.Table<Owner>().Where(o => o.ID == id).First();
        }

        public int OwnerExists(string name)
        {
            try
            {
               return _database.Table<Owner>().Where(o => o.Name.ToLower().Contains(name.ToLower())).First().ID;
            }
            catch (Exception e)
            {
                Log.Error($"Could not find Owner from the given name ({name}) \n Exception: {e}");
            }

            return 0;
        }

        public bool DeleteOwner(int id)
        {
            try
            {
                return _database.Delete<Owner>(id) != 0;
            }
            catch (Exception e)
            {
                Log.Error($"Could not delete Owner from the given id ({id}) \n Exception: {e}");
            }

            return false;
        }
        #endregion

        #region Claim
        public void InsertClaim(Claim claim)
        {
            _database.Insert(claim);
        }

        public List<Claim> RetrieveClaims(int owner)
        {
            return _database.Table<Claim>().Where(c => c.Owner == owner).ToList();
        }

        public Waifu RetrivedClaimedWaifu(int claim)
        {
            return _database.Table<Waifu>().Where(w => w.ID == claim).First();
        }

        public Owner RetrieveWaifuOwner(int waifu)
        {
            return _database.Table<Owner>().Where(o => o.ID == _database.Table<Claim>().Where(c => c.Waifu == waifu).First().Owner).First();
        }

        public bool DeleteClaim(int claim)
        {
            return _database.Delete<Claim>(claim) != 0;
        }
        #endregion
    }
}
