﻿using Newtonsoft.Json.Linq;
using SQLite;
using System;
using System.Collections.Generic;

namespace WaifuCentral_Lib.Structure
{
    public class Waifu
    {
        /// <summary>
        /// The internal ID used by WaifuCentral DataBase
        /// </summary>
        [PrimaryKey, AutoIncrement, NotNull]
        public int ID { get; set; }

        /// <summary>
        /// The Name of the Waifu
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Native Name (usually in kanji) of the Waifu
        /// </summary>
        public string NativeName { get; set; }

        /// <summary>
        /// The Alternative titles of the Waifu
        /// </summary>
        public string AlternativeTitles { get; set; }

        /// <summary>
        /// The Series where the Waifu is from
        /// </summary>
        public string Series { get; set; }

        /// <summary>
        /// The Description of the waifu
        /// </summary>
        [MaxLength(16384)]
        public string Description { get; set; }

        /// <summary>
        /// Image links of the waifu
        /// </summary>
        public JArray Images { get; set; }

        /// <summary>
        /// From where the info was retrived
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Creates a complete Waifu object (without id)
        /// </summary>
        /// <param name="name">The Name of the Waifu</param>
        /// <param name="nativename">The Native Name of the Waifu</param>
        /// <param name="alternativetitles">The Alternative Titles of the Waifu</param>
        /// <param name="series">The Origin Series of the Waifu</param>
        /// <param name="description">The Description of the Waifu</param>
        /// <param name="images">The Images of the Waifu</param>
        /// <param name="source">The Source where the Waifu's info was extracted</param>
        public Waifu(string name, string nativename, string alternativetitles, string series, string description, JArray images, string source)
        {
            this.Name = name;
            this.NativeName = nativename;
            this.AlternativeTitles = alternativetitles;
            this.Series = series;
            this.Description = description;
            this.Images = images;
            this.Source = source;
        }

        /// <summary>
        /// Creates a complete Waifu object
        /// </summary>
        /// <param name="id">The ID of the Waifu</param>
        /// <param name="name">The Name of the Waifu</param>
        /// <param name="nativename">The Native Name of the Waifu</param>
        /// <param name="alternativetitles">The Alternative Titles of the Waifu</param>
        /// <param name="series">The Origin Series of the Waifu</param>
        /// <param name="description">The Description of the Waifu</param>
        /// <param name="images">The Images of the Waifu</param>
        /// <param name="source">The Source where the Waifu's info was extracted</param>
        public Waifu(int id, string name, string nativename, string alternativetitles, string series, string description, JArray images, string source) : this(name, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty)
        {
            this.ID = id;
        }

        /// <summary>
        /// Default constructor, only used for internal uses as it sets everything to 0, Empty or Null. DO NOT USE THIS
        /// </summary>
        public Waifu() : this(String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty)
        {
        }

        /// <summary>
        /// Creates a Waifu object with the given name and ID
        /// </summary>
        /// <param name="id">The ID of the Waifu</param>
        /// <param name="name">The Name of the Waifu</param>
        public Waifu(int id, string name) : this(id, name, String.Empty, String.Empty, String.Empty, String.Empty, null, String.Empty)
        {
        }
   
    }

    public static class WaifuExtensions
    {
        public static bool IsWaifuNullOrEmpty(this Waifu waifu)
        {
            bool result = false;

            if(waifu != null)
            {
                if (String.IsNullOrWhiteSpace(waifu.Name) || waifu.Name == "NA")
                    result = true;
            }
            else
            {
                result = true;
            }

            return result;
        }

        public static string ToJson(this Waifu waifu)
        {
            return JObject.FromObject(waifu).ToString();
        }
    }
}
